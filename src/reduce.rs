//TODO: fallible reduction?
pub trait Reduce {
    /// The modes supported by this term
    type Mode: ReductionMode + Clone;
    /// The terms this form of term reduces to
    type Redex: Reduce<Mode = Self::Mode, Redex = Self::Redex>;
    /// Possible reduction errors
    type ReductionError;
    /// Reduce this term
    fn reduce(&self) -> Option<Self::Redex>
    where
        Self::Mode: Default,
    {
        self.reduce_with(Self::Mode::default())
    }
    /// Reduce this term with a given mode
    fn reduce_with(&self, mode: Self::Mode) -> Option<Self::Redex>;
    /// Normalize this term
    fn normalize(&self) -> Option<Self::Redex>
    where
        Self::Mode: Default + Clone,
    {
        self.normalize_with(Self::Mode::default())
    }
    /// Normalize this term with a given mode
    fn normalize_with(&self, mode: Self::Mode) -> Option<Self::Redex>
    where
        Self::Mode: Clone,
    {
        let mut curr = self.reduce_with(mode.clone())?;
        while let Some(redex) = curr.reduce_with(mode.clone()) {
            curr = redex
        }
        Some(curr)
    }
    /// Match this term to a zero variable
    fn match_zero(&self) -> bool {
        false
    }
    /// Match this term to an application
    fn match_app(&self) -> Option<(&Self::Redex, &Self::Redex)> {
        None
    }
    /// Match this term to an abstraction
    fn match_abs(&self) -> Option<&Self::Redex> {
        None
    }
}

/// A reduction mode
pub trait ReductionMode {
    /// Whether eta reductions are to be performed
    fn eta(&self) -> bool;
    /// Whether arbitrary beta reductions are to be performed
    fn beta(&self) -> bool;
    /// Whether arbitrary head reductions are to be performed
    fn head(&self) -> bool;
    /// Whether arbitrary weak head reductions are to be performed
    fn weak_head(&self) -> bool;
}