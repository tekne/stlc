/*!
A term supporting de Bruijn style substitution
*/
use super::*;

pub trait Substitute {
    /// The base term of this type
    type Term: Substitute<Term = Self::Term, Error = Self::Error> + Clone;
    /// The variable term of this type
    type Var: Variable<Self::Term>;
    /// The substitution error of this type
    type Error;
    /// Recursively transform this term
    fn map<M>(&self, map: M) -> Option<Self::Term>
    where
        M: FnMut(u64, &Self::Var) -> Option<Self::Term>,
        Self::Error: Into<Infallible>,
    {
        self.subst(map, |_, _| true, 0)
    }
    /// Get this term with an argument substituted in
    fn subst_arg(&self, arg: &Self::Term) -> Option<Self::Term>
    where
        Self::Error: Into<Infallible>,
    {
        self.try_subst_arg(arg).aok()
    }
    /// Get this term with an argument substituted in
    fn try_subst_arg(&self, arg: &Self::Term) -> Result<Option<Self::Term>, Self::Error> {
        self.try_fallible_subst(
            |depth, var| -> Result<Option<Self::Term>, Self::Error> {
                if var.matches(arg, depth)? {
                    let shifted = arg.try_shift_up(depth)?.unwrap_or_else(|| arg.clone());
                    return Ok(Some(shifted));
                }
                Ok(var.shift_down_at_depth(1, depth).unwrap_or(None))
            },
            |depth, term| {
                if depth >= term.upper_bound() {
                    Ok(false)
                } else {
                    Ok(true)
                }
            },
            0,
        )
        .m_right()
    }
    /// Recursively transform this term
    fn subst<M, F>(&self, map: M, filter: F, depth: u64) -> Option<Self::Term>
    where
        M: FnMut(u64, &Self::Var) -> Option<Self::Term>,
        F: FnMut(u64, &Self::Term) -> bool,
        Self::Error: Into<Infallible>,
    {
        self.try_subst(map, filter, depth).map_err(Into::into).aok()
    }
    /// Fallibly transform this term
    fn fallible_subst<M, F, E>(
        &self,
        map: M,
        filter: F,
        depth: u64,
    ) -> Result<Option<Self::Term>, E>
    where
        M: FnMut(u64, &Self::Var) -> Result<Option<Self::Term>, E>,
        F: FnMut(u64, &Self::Term) -> Result<bool, E>,
        Self::Error: Into<Infallible>,
    {
        self.try_fallible_subst(map, filter, depth).rok()
    }
    /// Attempt to recursively transform this term
    fn try_subst<M, F>(
        &self,
        mut map: M,
        mut filter: F,
        depth: u64,
    ) -> Result<Option<Self::Term>, Self::Error>
    where
        M: FnMut(u64, &Self::Var) -> Option<Self::Term>,
        F: FnMut(u64, &Self::Term) -> bool,
    {
        match self.try_fallible_subst_impl(
            &mut move |depth, var| -> Result<Option<Self::Term>, Infallible> {
                Ok(map(depth, var))
            },
            &mut move |depth, term| -> Result<bool, Infallible> { Ok(filter(depth, term)) },
            depth,
        ) {
            Ok(res) => Ok(res),
            Err(Either::Left(err)) => match err {},
            Err(Either::Right(err)) => Err(err),
        }
    }
    /// Attempt to recursively transform this term
    fn try_fallible_subst<M, F, E>(
        &self,
        mut map: M,
        mut filter: F,
        depth: u64,
    ) -> Result<Option<Self::Term>, Either<E, Self::Error>>
    where
        M: FnMut(u64, &Self::Var) -> Result<Option<Self::Term>, E>,
        F: FnMut(u64, &Self::Term) -> Result<bool, E>,
    {
        self.try_fallible_subst_impl(
            &mut move |depth, var| -> Result<Option<Self::Term>, E> { map(depth, var) },
            &mut move |depth, term| -> Result<bool, E> { filter(depth, term) },
            depth,
        )
    }
    /// Attempt to recursively transform this term
    fn try_fallible_subst_impl<M, F, E>(
        &self,
        map: &mut M,
        filter: &mut F,
        depth: u64,
    ) -> Result<Option<Self::Term>, Either<E, Self::Error>>
    where
        M: FnMut(u64, &Self::Var) -> Result<Option<Self::Term>, E>,
        F: FnMut(u64, &Self::Term) -> Result<bool, E>;
    /// The lower bound on this term's parameter indices. May not be exact.
    fn lower_bound(&self) -> u64;
    /// The upper bound on this term's parameter indices, plus 1. Guaranteed to be exact.
    fn upper_bound(&self) -> u64;
    /// Whether this term's lower bound is exact
    fn lower_bound_exact(&self) -> bool {
        false
    }
    /// Whether this term's upper bound is exact
    fn upper_bound_exact(&self) -> bool {
        false
    }
    /// Whether this term has a parameter
    fn has_param(&self) -> bool {
        self.upper_bound() > 0 && self.upper_bound_exact() || self.lower_bound() > 0
    }
    /// Whether this term is active, i.e. potentially depends on the topmost parameter
    fn is_active(&self) -> bool {
        self.lower_bound() == 0 && self.has_param()
    }

    /// Shift this term up, i.e. move all unbound references a given depth up the stack
    fn shift_up(&self, shift: u64) -> Option<Self::Term>
    where
        Self::Error: Into<Infallible>,
    {
        self.try_shift_up(shift).aok()
    }
    /// Shift this term up, i.e. move all unbound references a given depth up the stack
    fn try_shift_up(&self, shift: u64) -> Result<Option<Self::Term>, Self::Error> {
        if shift == 0 {
            return Ok(None);
        }
        self.try_subst(
            |depth, var| var.shift_up_at_depth(shift, depth),
            |depth, term| term.upper_bound() > depth,
            0,
        )
    }
    /// Shift this term down, i.e. move all unbound references a given depth down the stack.
    /// Return an error on underflow
    fn shift_down(&self, shift: u64) -> Result<Option<Self::Term>, u64>
    where
        Self::Error: Into<Infallible>,
    {
        self.try_shift_down(shift).rok()
    }
    /// Shift this term down, i.e. move all unbound references a given depth down the stack.
    /// Return an error on underflow
    fn try_shift_down(&self, shift: u64) -> Result<Option<Self::Term>, Either<u64, Self::Error>> {
        if shift == 0 {
            return Ok(None);
        }
        self.try_fallible_subst(
            |depth, var| var.shift_down_at_depth(shift + depth, depth),
            |depth, term| {
                let upper_bound = term.upper_bound();
                if upper_bound > depth + shift {
                    Ok(true)
                } else if upper_bound > depth {
                    Err(upper_bound)
                } else {
                    Ok(false)
                }
            },
            0,
        )
    }
}

/// A variable, which can be *infallibly* shifted up and down *at a given depth* to yield a term of a given type
pub trait Variable<Term: Substitute<Term = Term> + Clone> {
    /// Shift this variable up at a given depth
    fn shift_up_at_depth(&self, shift: u64, depth: u64) -> Option<Term>;
    /// Shift this variable down at a given depth
    fn shift_down_at_depth(&self, shift: u64, depth: u64) -> Result<Option<Term>, u64>;
    /// Try to match a variable at a given depth
    fn matches(&self, arg: &Term, depth: u64) -> Result<bool, Term::Error>;
    /// Try to substitute for a variable at a given depth
    fn try_subst_for(&self, arg: &Term, depth: u64) -> Result<Option<Term>, Term::Error> {
        if self.matches(arg, depth)? {
            let shifted = arg
                .try_shift_up(depth)
                .map(|shifted| shifted.unwrap_or_else(|| arg.clone()))
                .map(Some);
            shifted
        } else {
            Ok(self.shift_down_at_depth(1, depth).unwrap_or(None))
        }
    }
    /// Substitute for a variable at a given depth
    fn subst_for(&self, arg: &Term, depth: u64) -> Option<Term>
    where
        Term::Error: Into<Infallible>,
    {
        match self.try_subst_for(arg, depth) {
            Ok(term) => term,
            Err(err) => match err.into() {},
        }
    }
}

/// Infallible result trait
pub trait InfallibleResult {
    type Inner;
    fn aok(self) -> Self::Inner;
}

impl<T, E> InfallibleResult for Result<T, E>
where
    E: Into<Infallible>,
{
    type Inner = T;
    fn aok(self) -> Self::Inner {
        match self {
            Ok(res) => res,
            Err(err) => match err.into() {},
        }
    }
}

/// Infallible either trait
pub trait InfallibleEither {
    type Inner;
    type Left;
    type Right;
    fn rok(self) -> Result<Self::Inner, Self::Left>
    where
        Self::Right: Into<Infallible>;
    fn lok(self) -> Result<Self::Inner, Self::Right>
    where
        Self::Left: Into<Infallible>;
    fn m_right(self) -> Result<Self::Inner, Self::Right>
    where
        Self::Left: Into<Self::Right>;
}

impl<T, L, R> InfallibleEither for Result<T, Either<L, R>> {
    type Inner = T;
    type Left = L;
    type Right = R;
    fn rok(self) -> Result<Self::Inner, Self::Left>
    where
        Self::Right: Into<Infallible>,
    {
        match self {
            Ok(res) => Ok(res),
            Err(Either::Left(err)) => Err(err),
            Err(Either::Right(err)) => match err.into() {},
        }
    }
    fn lok(self) -> Result<Self::Inner, Self::Right>
    where
        Self::Left: Into<Infallible>,
    {
        match self {
            Ok(res) => Ok(res),
            Err(Either::Right(err)) => Err(err),
            Err(Either::Left(err)) => match err.into() {},
        }
    }
    fn m_right(self) -> Result<Self::Inner, Self::Right>
    where
        Self::Left: Into<Self::Right>,
    {
        match self {
            Ok(res) => Ok(res),
            Err(Either::Right(err)) => Err(err),
            Err(Either::Left(err)) => Err(err.into()),
        }
    }
}
