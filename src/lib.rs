use ahash::AHasher;
use either::Either;
use elysees::Arc;
use lazy_static::lazy_static;
use std::convert::Infallible;
use std::hash::{Hash, Hasher};
use std::ops::*;

pub mod lambda;
pub mod parser;
pub mod prettyprinter;
pub mod simple_types;
pub mod sk;

mod reduce;
mod subst;
pub use reduce::*;
pub use subst::*;
