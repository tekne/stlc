use super::*;

/// A type in the simply typed lambda calculus
#[derive(Eq, Hash, PartialEq)]
pub enum Type {
    /// An indexed atomic type
    Var(u64),
    /// A function type
    Func(Func),
}

/// A function type
#[derive(Eq)]
pub struct Func {
    /// The domain of this function type
    domain: Arc<Type>,
    /// The target of this function type
    target: Arc<Type>,
    /// The code of this function type
    code: u64,
}

impl PartialEq for Func {
    fn eq(&self, other: &Func) -> bool {
        self.code == other.code && self.domain == other.domain && self.target == other.target
    }
}

impl Hash for Func {
    fn hash<H: Hasher>(&self, hasher: &mut H) {
        self.code.hash(hasher)
    }
}

impl Func {
    /// Create a new function type
    pub fn new(domain: Arc<Type>, target: Arc<Type>) -> Func {
        let mut result = Func {
            domain,
            target,
            code: 0,
        };
        result.update_code();
        result
    }
    /// Get the domain of this function type
    pub fn domain(&self) -> &Type {
        &self.domain
    }
    /// Get the target of this function type
    pub fn target(&self) -> &Type {
        &self.target
    }
    /// Get this function type's code
    pub fn code(&self) -> u64 {
        self.code
    }
    /// Get the hasher used for function types
    fn get_hasher() -> impl Hasher {
        AHasher::new_with_keys(452, 742)
    }
    /// Update this function type's code
    fn update_code(&mut self) {
        let mut hasher = Self::get_hasher();
        self.domain.hash(&mut hasher);
        self.target.hash(&mut hasher);
        self.code = hasher.finish()
    }
}
