use super::*;
use lambda::*;
use simple_types::*;
use std::fmt::*;

/// A trait for something Prettyprint with a given printer type
pub trait Prettyprint<P> {
    /// Prettyprint this object
    fn prettyprint(&self, printer: P, fmt: &mut Formatter) -> Result;
    /// Construct an object which formats as this object being prettyprinted
    fn prp(&self, printer: P) -> Prp<Self, P>
    where
        Self: Sized,
    {
        Prp(self, printer)
    }
}

/// A struct which prettyprints an object
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
pub struct Prp<'a, O, P>(pub &'a O, pub P);

impl<'a, O, P> Display for Prp<'a, O, P>
where
    O: Prettyprint<P>,
    P: Clone,
{
    fn fmt(&self, fmt: &mut Formatter) -> Result {
        self.0.prettyprint(self.1.clone(), fmt)
    }
}

/// A printing style
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
pub enum PrintingStyle {
    /// Always bracketed
    Bracketed,
    /// Never bracketed (the outer layer)
    Outer,
    /// Always bracketed (the outer layer)
    Inner,
    /// Associative things bracketed
    Assoc,
    /// Operators bracketed
    Operator,
}

use PrintingStyle::*;

impl PrintingStyle {
    /// A list of all printing styles
    pub const PRINTING_STYLES: &'static [PrintingStyle] =
        &[Bracketed, Outer, Inner, Assoc, Operator];
    /// Whether this has associative brackets
    pub fn is_assoc(self) -> bool {
        matches!(self, Bracketed | Assoc | Inner)
    }
    /// Whether this has operator brackets
    pub fn is_operator(self) -> bool {
        matches!(self, Bracketed | Operator | Inner)
    }
    /// Toggle brackets to associative brackets
    pub fn assoc(self) -> PrintingStyle {
        match self {
            Bracketed => Bracketed,
            _ => Assoc,
        }
    }
    /// Toggle to operator brackets
    pub fn operator(self) -> PrintingStyle {
        match self {
            Bracketed => Bracketed,
            _ => Operator,
        }
    }
}

impl Prettyprint<PrintingStyle> for Type {
    fn prettyprint(&self, style: PrintingStyle, fmt: &mut Formatter) -> Result {
        use Type::*;
        match self {
            Var(a) => Display::fmt(a, fmt),
            Func(f) => f.prettyprint(style, fmt),
        }
    }
}

impl Prettyprint<PrintingStyle> for Func {
    fn prettyprint(&self, style: PrintingStyle, fmt: &mut Formatter) -> Result {
        if style.is_assoc() {
            write!(fmt, "(")?
        }
        self.domain().prettyprint(style.assoc(), fmt)?;
        write!(fmt, " -> ")?;
        self.target().prettyprint(style.operator(), fmt)?;
        if style.is_assoc() {
            write!(fmt, ")")?
        }
        Ok(())
    }
}

impl Prettyprint<PrintingStyle> for Term {
    fn prettyprint(&self, style: PrintingStyle, fmt: &mut Formatter) -> Result {
        use Term::*;
        match self {
            Var(v) => Display::fmt(v, fmt),
            App(a) => a.prettyprint(style, fmt),
            Abs(a) => a.prettyprint(style, fmt),
        }
    }
}

impl Prettyprint<PrintingStyle> for App {
    fn prettyprint(&self, style: PrintingStyle, fmt: &mut Formatter) -> Result {
        if style.is_assoc() {
            write!(fmt, "(")?
        }
        self.func().prettyprint(style.operator(), fmt)?;
        write!(fmt, " ")?;
        self.arg().prettyprint(style.assoc(), fmt)?;
        if style.is_assoc() {
            write!(fmt, ")")?
        }
        Ok(())
    }
}

impl Prettyprint<PrintingStyle> for Abs {
    fn prettyprint(&self, style: PrintingStyle, fmt: &mut Formatter) -> Result {
        if style.is_operator() {
            write!(fmt, "(")?
        }
        write!(fmt, "λ ")?;
        self.abs().prettyprint(style, fmt)?;
        if style.is_operator() {
            write!(fmt, ")")?
        }
        Ok(())
    }
}

impl Prettyprint<PrintingStyle> for sk::Term {
    fn prettyprint(&self, style: PrintingStyle, fmt: &mut Formatter) -> Result {
        match self {
            sk::Term::App(a) => a.prettyprint(style, fmt),
            sk::Term::Var(v) => Display::fmt(v, fmt),
            sk::Term::K => Display::fmt("K", fmt),
            sk::Term::S => Display::fmt("S", fmt),
        }
    }
}

impl Prettyprint<PrintingStyle> for sk::App {
    fn prettyprint(&self, style: PrintingStyle, fmt: &mut Formatter) -> Result {
        if style.is_assoc() {
            write!(fmt, "(")?
        }
        self.func().prettyprint(style.operator(), fmt)?;
        write!(fmt, " ")?;
        self.arg().prettyprint(style.assoc(), fmt)?;
        if style.is_assoc() {
            write!(fmt, ")")?
        }
        Ok(())
    }
}

impl Display for Type {
    fn fmt(&self, fmt: &mut Formatter) -> Result {
        use Type::*;
        match self {
            Var(a) => Display::fmt(a, fmt),
            Func(f) => Display::fmt(f, fmt),
        }
    }
}

impl Display for Func {
    fn fmt(&self, fmt: &mut Formatter) -> Result {
        self.prettyprint(PrintingStyle::Bracketed, fmt)
    }
}

impl Display for Term {
    fn fmt(&self, fmt: &mut Formatter) -> Result {
        self.prettyprint(PrintingStyle::Bracketed, fmt)
    }
}

impl Display for App {
    fn fmt(&self, fmt: &mut Formatter) -> Result {
        self.prettyprint(PrintingStyle::Bracketed, fmt)
    }
}

impl Display for Abs {
    fn fmt(&self, fmt: &mut Formatter) -> Result {
        self.prettyprint(PrintingStyle::Bracketed, fmt)
    }
}

impl Display for sk::Term {
    fn fmt(&self, fmt: &mut Formatter) -> Result {
        self.prettyprint(PrintingStyle::Bracketed, fmt)
    }
}

impl Display for sk::App {
    fn fmt(&self, fmt: &mut Formatter) -> Result {
        self.prettyprint(PrintingStyle::Bracketed, fmt)
    }
}

impl Debug for Type {
    fn fmt(&self, fmt: &mut Formatter) -> Result {
        self.prettyprint(PrintingStyle::Bracketed, fmt)
    }
}

impl Debug for Func {
    fn fmt(&self, fmt: &mut Formatter) -> Result {
        self.prettyprint(PrintingStyle::Bracketed, fmt)
    }
}

impl Debug for Term {
    fn fmt(&self, fmt: &mut Formatter) -> Result {
        self.prettyprint(PrintingStyle::Bracketed, fmt)
    }
}

impl Debug for App {
    fn fmt(&self, fmt: &mut Formatter) -> Result {
        self.prettyprint(PrintingStyle::Bracketed, fmt)
    }
}

impl Debug for Abs {
    fn fmt(&self, fmt: &mut Formatter) -> Result {
        self.prettyprint(PrintingStyle::Bracketed, fmt)
    }
}

impl Debug for sk::Term {
    fn fmt(&self, fmt: &mut Formatter) -> Result {
        self.prettyprint(PrintingStyle::Bracketed, fmt)
    }
}

impl Debug for sk::App {
    fn fmt(&self, fmt: &mut Formatter) -> Result {
        self.prettyprint(PrintingStyle::Bracketed, fmt)
    }
}
