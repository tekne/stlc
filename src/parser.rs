use super::*;
use lambda::*;
use nom::{
    self,
    branch::alt,
    bytes::complete::tag,
    character::complete::{digit1, multispace1},
    combinator::{map, map_res, opt},
    sequence::{delimited, preceded},
    IResult,
};
use simple_types::*;

/// Parse a type
pub fn ty(input: &str) -> IResult<&str, Type> {
    let (mut rest, mut curr) = atom_ty(input)?;
    loop {
        let (rem, arg) = match preceded(delimited(opt(ws), arrow, opt(ws)), atom_ty)(rest) {
            Ok(arg) => arg,
            Err(nom::Err::Incomplete(n)) => return Err(nom::Err::Incomplete(n)),
            _ => break,
        };
        rest = rem;
        curr = Type::Func(Func::new(Arc::new(curr), Arc::new(arg)))
    }
    Ok((rest, curr))
}

/// Parse an atomic type
pub fn atom_ty(input: &str) -> IResult<&str, Type> {
    alt((
        delimited(preceded(tag("("), opt(ws)), ty, preceded(opt(ws), tag(")"))),
        map(ix, Type::Var),
    ))(input)
}

/// Parse a term
pub fn term(input: &str) -> IResult<&str, Term> {
    let (mut rest, mut curr) = atom(input)?;
    loop {
        let (rem, arg) = match preceded(opt(ws), atom)(rest) {
            Ok(arg) => arg,
            Err(nom::Err::Incomplete(n)) => return Err(nom::Err::Incomplete(n)),
            _ => break,
        };
        rest = rem;
        curr = Term::from(App::new(curr, arg))
    }
    Ok((rest, curr))
}

/// Parse a full term; panic if the entire string is not consumed or on parse failure
pub fn full_term(input: &str) -> Term {
    let (rest, term) = term(input).unwrap();
    assert_eq!(rest, "");
    term
}

/// Parse an atomic term
pub fn atom(input: &str) -> IResult<&str, Term> {
    alt((
        delimited(
            preceded(tag("("), opt(ws)),
            term,
            preceded(opt(ws), tag(")")),
        ),
        map(abs, Term::from),
        map(ix, Term::from),
    ))(input)
}

/// Parse an abstraction
pub fn abs(input: &str) -> IResult<&str, Abs> {
    map(preceded(lambda, preceded(opt(ws), term)), |term| {
        Abs::new(term)
    })(input)
}

/// Parse an index
pub fn ix(input: &str) -> IResult<&str, u64> {
    map_res(digit1, str::parse)(input)
}

/// Parse whitespace
pub fn ws(input: &str) -> IResult<&str, &str> {
    multispace1(input)
}

/// Parse a lambda
pub fn lambda(input: &str) -> IResult<&str, &str> {
    alt((tag("\\\\"), tag("λ")))(input)
}

/// Parse an arrow
pub fn arrow(input: &str) -> IResult<&str, &str> {
    tag("->")(input)
}

#[cfg(test)]
mod test {
    use super::*;
    use pretty_assertions::assert_eq;
    use prettyprinter::*;
    #[test]
    fn lambda_variants() {
        assert_eq!(term("\\\\ 1").unwrap(), term("λ 1").unwrap());
    }
    #[test]
    fn basic_round_trip() {
        let exprs = [
            "λ λ λ 2 0 (1 0)",        // S
            "λ λ 1",                   // K
            "λ 0",                      // I
            "λ (λ 0 (λ 0)) (λ 1 0)", // Lifted off Wikipedia...
            "λ λ λ 0 (1 2)",          // B
            "λ λ λ 0 2 1",            // C
            "λ λ 0",                   // F
            "(λ 0 0)(λ 0 0)",          // Omega
        ];
        for &expr in &exprs {
            let (rest, parsed) = term(expr).unwrap();
            assert_eq!(rest, "");
            for &style in PrintingStyle::PRINTING_STYLES {
                let f = format!("{}", parsed.prp(style));
                let (rest, f_parsed) = term(&f).unwrap();
                assert_eq!(rest, "");
                assert_eq!(parsed, f_parsed);
                let f2 = format!("{}", f_parsed.prp(style));
                assert_eq!(f2, f);
            }
        }
    }
}
