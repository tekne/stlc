/*!
An implementation of combinatory logic with variable placeholders
*/
use super::*;

/// A combinatory logic term
#[derive(Clone, Eq, PartialEq, Hash)]
pub enum Term {
    /// An application
    App(Arc<App>),
    /// A variable
    Var(u64),
    /// The S combinator
    S,
    /// The K combinator
    K,
}

use Term::*;

impl Mul for Term {
    type Output = Term;
    fn mul(self, other: Term) -> Term {
        App::new(self, other).into()
    }
}

impl Mul<&'_ Term> for Term {
    type Output = Term;
    fn mul(self, other: &Term) -> Term {
        App::new(self, other.clone()).into()
    }
}

impl Mul<Term> for &'_ Term {
    type Output = Term;
    fn mul(self, other: Term) -> Term {
        App::new(self.clone(), other).into()
    }
}

impl Mul for &'_ Term {
    type Output = Term;
    fn mul(self, other: &Term) -> Term {
        App::new(self.clone(), other.clone()).into()
    }
}

impl From<u64> for Term {
    fn from(var: u64) -> Term {
        Var(var)
    }
}

impl From<App> for Term {
    fn from(app: App) -> Term {
        App(Arc::new(app))
    }
}

impl Term {
    /// Get the code of this term
    pub fn code(&self) -> u64 {
        match self {
            S => !0x83,
            K => !0x75,
            Var(v) => *v,
            App(a) => a.code(),
        }
    }
    /// Reduce this term
    pub fn reduce(&self, mode: RMode) -> Option<Term> {
        match self {
            App(a) => a.reduce(mode),
            _ => None,
        }
    }
    /// Recursively transform this term
    pub fn subst<M: FnMut(u64) -> Option<Term>>(&self, map: &mut M) -> Option<Term> {
        match self {
            App(a) => a.subst(map),
            Var(v) => map(*v),
            _ => None,
        }
    }
    /// Shift this term, i.e. move all unbound (above min) references a given depth down the stack
    pub fn shift(&self, depth: u64) -> Option<Term> {
        if depth == 0 {
            return None;
        }
        self.subst(&mut |v| Some(Term::Var(v - depth)))
    }
    /// Attempt to normalize this term. Return `None` if this term is already in normal form
    pub fn normalize(&self, mode: RMode) -> Option<Term> {
        let mut curr = self.reduce(mode)?;
        while let Some(redex) = curr.reduce(mode) {
            curr = redex
        }
        Some(curr)
    }
    /// Get the normal form of this term. Will enter an infinite loop if there is none
    pub fn normal(&self) -> Term {
        self.normalize(RMode::SK).unwrap_or_else(|| self.clone())
    }
    /// Match an active term
    pub fn match_active(&self) -> Option<ActiveTerm> {
        match self {
            App(a) => a.match_active(),
            _ => None,
        }
    }
    /// Match a semi-active term
    pub fn match_semiactive(&self) -> Option<SemiactiveTerm> {
        match self {
            App(a) => a.match_semiactive(),
            K => Some(SemiactiveTerm::K),
            _ => None,
        }
    }
    /// Whether this term has a free zero
    pub fn has_zero(&self) -> bool {
        match self {
            App(a) => a.has_zero(),
            Var(v) => *v == 0,
            S => false,
            K => false,
        }
    }
    /// Whether this term has a free variable
    pub fn has_free(&self) -> bool {
        match self {
            App(a) => a.has_free(),
            Var(_) => true,
            S => false,
            K => false,
        }
    }
}

/// An "active term"
#[derive(Clone, Eq, PartialEq, Hash)]
pub enum ActiveTerm {
    KX(Term),
    SXY(Term, Term),
}

impl ActiveTerm {
    /// Apply an active term to a term to obtain a term
    pub fn apply(self, z: &Term) -> Term {
        match self {
            ActiveTerm::KX(term) => term,
            ActiveTerm::SXY(x, y) => {
                App::new(App::new(x, z.clone()).into(), App::new(y, z.clone()).into()).into()
            }
        }
    }
}

/// A "semiactive term"
#[derive(Clone, Eq, PartialEq, Hash)]
pub enum SemiactiveTerm {
    K,
    SX(Term),
}

impl SemiactiveTerm {
    /// Apply a semiactive term to a term to obtain an active term
    pub fn apply(self, right: Term) -> ActiveTerm {
        match self {
            SemiactiveTerm::K => ActiveTerm::KX(right),
            SemiactiveTerm::SX(left) => ActiveTerm::SXY(left, right),
        }
    }
}

/// An application of combinatory logic terms
#[derive(Clone, Eq, PartialEq)]
pub struct App {
    /// The code of this application
    code: u64,
    /// The left term
    func: Term,
    /// The right term
    arg: Term,
}

impl Hash for App {
    fn hash<H>(&self, hasher: &mut H)
    where
        H: Hasher,
    {
        self.code.hash(hasher)
    }
}

impl App {
    /// Construct a new application.
    pub fn new(func: Term, arg: Term) -> App {
        let has_free = func.has_free() | arg.has_free();
        let has_zero = func.has_zero() | arg.has_zero();
        let mut result = App { code: 0, func, arg };
        result.update_code(has_free, has_zero);
        result
    }
    /// Get the function being applied
    pub fn func(&self) -> &Term {
        &self.func
    }
    /// Get the argument of this application
    pub fn arg(&self) -> &Term {
        &self.arg
    }
    /// Get the code of this application
    pub fn code(&self) -> u64 {
        self.code
    }
    /// Get the hasher used for applications
    pub fn get_hasher() -> impl Hasher {
        AHasher::new_with_keys(435, 131)
    }
    /// Update the code of this application
    fn update_code(&mut self, has_free: bool, has_zero: bool) {
        let mut hasher = Self::get_hasher();
        self.func.hash(&mut hasher);
        self.arg.hash(&mut hasher);
        self.code = hasher.finish();
        self.code &= !0b11;
        if has_free {
            self.code |= 0b01;
        }
        if has_zero {
            self.code |= 0b10;
        }
    }
    /// Reduce this term
    pub fn reduce(&self, mode: RMode) -> Option<Term> {
        match mode {
            RMode::SK => self
                .func
                .match_active()
                .map(|active| active.apply(&self.arg)),
        }
    }
    /// Transform this application
    pub fn subst<M: FnMut(u64) -> Option<Term>>(&self, map: &mut M) -> Option<Term> {
        let func = self.func.subst(map);
        let arg = self.arg.subst(map);
        if func.is_none() && arg.is_none() {
            return None;
        }
        let func = func.unwrap_or_else(|| self.func.clone());
        let arg = arg.unwrap_or_else(|| self.arg.clone());
        Some(App::new(func, arg).into())
    }
    /// Shift this application up by n levels
    pub fn shift(&self, depth: u64) -> Option<Term> {
        if depth == 0 {
            return None;
        }
        self.subst(&mut |v| Some(Term::Var(v - depth)))
    }
    /// Attempt to normalize this term. Return `None` if this term is already in normal form
    pub fn normalize(&self, mode: RMode) -> Option<Term> {
        let mut curr = self.reduce(mode)?;
        while let Some(redex) = curr.reduce(mode) {
            curr = redex
        }
        Some(curr)
    }
    /// Match an active term
    pub fn match_active(&self) -> Option<ActiveTerm> {
        self.func
            .match_semiactive()
            .map(|semiactive| semiactive.apply(self.arg.clone()))
    }
    /// Match a semiactive term
    pub fn match_semiactive(&self) -> Option<SemiactiveTerm> {
        if self.func == S {
            Some(SemiactiveTerm::SX(self.arg.clone()))
        } else {
            None
        }
    }
    /// Whether this term has a free variable
    pub fn has_free(&self) -> bool {
        self.code & 0b01 != 0
    }
    /// Whether this term has a free zero
    pub fn has_zero(&self) -> bool {
        self.code & 0b10 != 0
    }
}

/// A reduction mode
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
pub enum RMode {
    /// Perform only standard combinatory logic reductions
    SK,
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn beta_normal_form() {
        let pairs = [
            (K * K * S, K),
            (
                S * Var(0) * Var(1) * Var(2),
                Var(0) * Var(2) * (Var(1) * Var(2)),
            ),
            (S * K * K * K, K),
            (S * K * K * S, S),
        ];
        for (term, normal) in pairs.iter() {
            assert_eq!(term.normal(), *normal)
        }
    }
    #[test]
    fn has_zero_free() {
        let zero = [Var(0), K * Var(0), Var(0) * Var(1)];
        for term in &zero {
            assert!(term.has_zero());
            assert!(term.has_free());
        }
        let free = [Var(1), K * Var(1), S * K * K * Var(1)];
        for term in &free {
            assert!(!term.has_zero());
            assert!(term.has_free())
        }
        let proprietary = [S, K, K * S, S * K * K];
        for term in &proprietary {
            assert!(!term.has_zero());
            assert!(!term.has_free());
        }
    }
}
