/*!
An implementation of the lambda calculus using de Bruijn indexing
*/
use super::*;

/// A term in the simply typed lambda calculus
#[derive(Clone, Eq, PartialEq, Hash)]
pub enum Term {
    /// An indexed variable, with a given type
    Var(u64),
    /// A function application
    App(Arc<App>),
    /// A lambda abstraction
    Abs(Arc<Abs>),
}

use Term::*;

lazy_static! {
    /// The S combinator
    pub static ref S: Term = Abs::new(
        Abs::new(
            Abs::new(
                App::new(
                    App::new(Var(2), Var(0)).into(),
                    App::new(Var(1), Var(0)).into()
                )
                .into()
            )
            .into()
        )
        .into()
    )
    .into();
    /// The K combinator
    pub static ref K: Term = Abs::new(Abs::new(Var(1)).into()).into();
    /// The I combinator
    pub static ref I: Term = Abs::new(Var(0)).into();
    /// The B combinator
    pub static ref B: Term = Abs::new(
        Abs::new(
            Abs::new(
                App::new(
                    Var(2),
                    App::new(Var(1), Var(0)).into()
                )
                .into()
            )
            .into()
        )
        .into()
    )
    .into();
    /// The C combinator
    pub static ref C: Term = Abs::new(
        Abs::new(
            Abs::new(
                App::new(
                    App::new(Var(2), Var(0)).into(),
                    Var(1),
                )
                .into()
            )
            .into()
        )
        .into()
    )
    .into();
    /// The T combinator
    pub static ref T: Term = K.clone();
    /// The F combinator
    pub static ref F: Term = Abs::new(I.clone()).into();
    /// Zero applied to itself
    pub static ref ZERO_SELF_APP: Term = App::new(Var(0), Var(0)).into();
    /// Self-application
    pub static ref SELF_APP: Term = Abs::new(ZERO_SELF_APP.clone()).into();
    /// The omega combinator
    pub static ref OMEGA: Term = App::new(SELF_APP.clone(), SELF_APP.clone()).into();
    /// Wrapped self-application
    pub static ref WRAP_SELF_APP: Term = Abs::new(App::new(Var(1), ZERO_SELF_APP.clone()).into()).into();
    /// The Y combinator
    pub static ref Y: Term = Abs::new(
        App::new(
            WRAP_SELF_APP.clone(),
            WRAP_SELF_APP.clone(),
        ).into()
    ).into();
}

impl Mul for Term {
    type Output = Term;
    fn mul(self, other: Term) -> Term {
        App::new(self, other).into()
    }
}

impl Mul<&'_ Term> for Term {
    type Output = Term;
    fn mul(self, other: &Term) -> Term {
        App::new(self, other.clone()).into()
    }
}

impl Mul<Term> for &'_ Term {
    type Output = Term;
    fn mul(self, other: Term) -> Term {
        App::new(self.clone(), other).into()
    }
}

impl Mul for &'_ Term {
    type Output = Term;
    fn mul(self, other: &Term) -> Term {
        App::new(self.clone(), other.clone()).into()
    }
}

impl From<u64> for Term {
    fn from(var: u64) -> Term {
        Var(var)
    }
}

impl From<App> for Term {
    fn from(app: App) -> Term {
        App(Arc::new(app))
    }
}

impl From<Abs> for Term {
    fn from(abs: Abs) -> Term {
        Abs(Arc::new(abs))
    }
}

impl Term {
    /// Get the code of this term
    pub fn code(&self) -> u64 {
        match self {
            Var(v) => *v,
            App(a) => a.code(),
            Abs(a) => a.code(),
        }
    }
}

impl Reduce for Term {
    type Mode = RMode;
    type Redex = Term;
    type ReductionError = Infallible;
    fn reduce_with(&self, mode: RMode) -> Option<Term> {
        match self {
            Var(_) => None,
            App(a) => a.reduce_with(mode),
            Abs(a) => a.reduce_with(mode),
        }
    }
    fn match_zero(&self) -> bool {
        match self {
            Var(v) => *v == 0,
            _ => false,
        }
    }
    fn match_app(&self) -> Option<(&Term, &Term)> {
        match self {
            App(a) => a.match_app(),
            _ => None,
        }
    }
    /// Match this term to an abstraction
    fn match_abs(&self) -> Option<&Self::Redex> {
        match self {
            Abs(a) => a.match_abs(),
            _ => None,
        }
    }
}

impl Variable<Term> for u64 {
    fn shift_up_at_depth(&self, shift: u64, depth: u64) -> Option<Term> {
        if *self >= depth {
            Some(Var(*self + shift))
        } else {
            None
        }
    }
    fn shift_down_at_depth(&self, shift: u64, depth: u64) -> Result<Option<Term>, u64> {
        if *self >= depth {
            self.checked_sub(shift).ok_or(*self).map(Var).map(Some)
        } else {
            Ok(None)
        }
    }
    fn matches(&self, _term: &Term, depth: u64) -> Result<bool, Infallible> {
        Ok(*self == depth)
    }
}

/// A function application
#[derive(Eq)]
pub struct App<T = Term> {
    /// The code of this application
    code: u64,
    /// The maximal parameter of this term plus one
    upper_bound: u64,
    /// The minimal parameter of this term
    lower_bound: u64,
    /// The value it is being applied to
    arg: T,
    /// The function being applied
    func: T,
}

impl<T: PartialEq> PartialEq for App<T> {
    fn eq(&self, other: &App<T>) -> bool {
        self.code == other.code && self.func == other.func && self.arg == other.arg
    }
}

impl Hash for App {
    fn hash<H: Hasher>(&self, hasher: &mut H) {
        self.code.hash(hasher)
    }
}

impl<T: Hash + Substitute> App<T> {
    /// Construct a new application.
    pub fn new(func: T, arg: T) -> App<T> {
        let upper_bound = func.upper_bound().max(arg.upper_bound());
        let lower_bound = func.lower_bound().min(arg.lower_bound());
        let mut result = App {
            code: 0,
            func,
            arg,
            upper_bound,
            lower_bound,
        };
        result.update_code();
        result
    }
    /// Get the function being applied
    pub fn func(&self) -> &T {
        &self.func
    }
    /// Get the argument of this application
    pub fn arg(&self) -> &T {
        &self.arg
    }
    /// Get the code of this application
    pub fn code(&self) -> u64 {
        self.code
    }
    /// Get the hasher used for applications
    pub fn get_hasher() -> impl Hasher {
        AHasher::new_with_keys(435, 131)
    }
    /// Update the code of this application
    fn update_code(&mut self) {
        let mut hasher = Self::get_hasher();
        self.func.hash(&mut hasher);
        self.arg.hash(&mut hasher);
        self.code = hasher.finish();
    }
}

impl<T> Reduce for App<T>
where
    T: Clone + Hash + Substitute<Term = T, Error = Infallible> + Reduce<Redex = T>,
    App<T>: Into<T>,
{
    type Mode = T::Mode;
    type Redex = T;
    type ReductionError = ();
    fn reduce_with(&self, mode: T::Mode) -> Option<T> {
        let redex = self
            .func
            .reduce_with(mode.clone())
            .map(|func_redex| App::new(func_redex, self.arg.clone()).into());
        if redex.is_some() {
            return redex;
        }
        if mode.weak_head() {
            if let Some(abs) = self.func.match_abs() {
                return Some(abs.subst_arg(&self.arg).unwrap_or_else(|| abs.clone()));
            }
        }
        if mode.beta() {
            self.arg
                .reduce_with(mode.clone())
                .map(|arg_redex| App::new(self.func.clone(), arg_redex).into())
        } else {
            None
        }
    }
    /// Match this term to an application
    fn match_app(&self) -> Option<(&T, &T)> {
        Some((&self.func, &self.arg))
    }
}

/// A lambda abstraction
#[derive(Eq)]
pub struct Abs<T = Term> {
    /// The term being abstracted over
    abs: T,
    /// The code of this lambda abstraction
    code: u64,
    /// The maximal parameter of this abstraction
    upper_bound: u64,
    /// The minimal parameter of this abstraction
    lower_bound: u64,
}

impl<T> Hash for Abs<T> {
    fn hash<H: Hasher>(&self, hasher: &mut H) {
        self.code.hash(hasher)
    }
}

impl<T: PartialEq> PartialEq for Abs<T> {
    fn eq(&self, other: &Abs<T>) -> bool {
        self.code == other.code && self.abs == other.abs
    }
}

impl<T> Abs<T>
where
    T: Hash + Substitute<Term = T> + Reduce<Redex = T> + Clone,
{
    /// Construct a new lambda abstraction over a term
    pub fn new(abs: T) -> Abs<T> {
        let upper_bound = abs.upper_bound().saturating_sub(1);
        let lower_bound = abs.lower_bound().saturating_sub(1);
        let mut result = Abs {
            abs,
            code: 0,
            upper_bound,
            lower_bound,
        };
        result.update_code();
        result
    }
    /// Get the term being abstracted over
    pub fn abs(&self) -> &T {
        &self.abs
    }
    /// Get the code of this lambda abstraction
    pub fn code(&self) -> u64 {
        self.code
    }
    /// Get the hasher used for abstractions
    pub fn get_hasher() -> impl Hasher {
        AHasher::new_with_keys(23, 33212)
    }
    /// Update the code of this lambda abstraction
    fn update_code(&mut self) {
        let mut hasher = Self::get_hasher();
        self.abs.hash(&mut hasher);
        self.code = hasher.finish();
    }
    /// Apply this abstraction to a given term
    pub fn apply(&self, arg: &T) -> T
    where
        T::Error: Into<Infallible>,
    {
        self.abs.subst_arg(arg).unwrap_or_else(|| self.abs.clone())
    }
    /// Eta-reduce this lambda abstraction
    pub fn eta_reduce(&self) -> Option<T>
    where
        T::Error: Into<Infallible>,
    {
        let (func, arg) = self.abs.match_app()?;
        if !arg.match_zero() {
            return None;
        }
        Some(func.shift_down(1).ok()?.unwrap_or_else(|| func.clone()))
    }
}

impl Reduce for Abs {
    type Mode = RMode;
    type Redex = Term;
    type ReductionError = ();
    fn reduce_with(&self, mode: RMode) -> Option<Term> {
        if mode.head() {
            let redex = self.abs.reduce_with(mode).map(Abs::new).map(From::from);
            if redex.is_some() {
                return redex;
            }
        }
        if mode.eta() {
            self.eta_reduce()
        } else {
            None
        }
    }
    fn match_abs(&self) -> Option<&Self::Redex> {
        Some(self.abs())
    }
}

/// A reduction mode
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
pub enum RMode {
    /// Perform arbitrary beta-eta reductions, leftmost beta first
    BetaEta,
    /// Perform arbitrary beta-reductions, leftmost first
    Beta,
    /// Perform eta reductions only
    Eta,
    /// Perform only head reductions
    Head,
    /// Perform only weak head reductions
    WeakHead,
}

use RMode::*;

impl RMode {
    /// A list of all possible reduction modes
    pub const RMODES: &'static [RMode] = &[BetaEta, Beta, Eta, Head, WeakHead];
}

/// A reduction mode
impl ReductionMode for RMode {
    /// Whether eta reductions are to be performed
    fn eta(&self) -> bool {
        matches!(self, Eta | BetaEta)
    }
    /// Whether arbitrary beta reductions are to be performed
    fn beta(&self) -> bool {
        matches!(self, Beta | BetaEta)
    }
    /// Whether arbitrary head reductions are to be performed
    fn head(&self) -> bool {
        matches!(self, Beta | BetaEta | Head)
    }
    /// Whether arbitrary weak head reductions are to be performed
    fn weak_head(&self) -> bool {
        matches!(self, Beta | BetaEta | Head | WeakHead)
    }
}

impl Substitute for Term {
    /// The base term of this type
    type Term = Self;
    /// The variable term of this type
    type Var = u64;
    /// The substitution error of this type
    type Error = Infallible;
    /// Attempt to recursively transform this term
    fn try_fallible_subst_impl<M, F, E>(
        &self,
        map: &mut M,
        filter: &mut F,
        depth: u64,
    ) -> Result<Option<Self::Term>, Either<E, Infallible>>
    where
        M: FnMut(u64, &u64) -> Result<Option<Term>, E>,
        F: FnMut(u64, &Term) -> Result<bool, E>,
    {
        if !filter(depth, self).map_err(Either::Left)? {
            return Ok(None);
        }
        match self {
            App(a) => a.try_fallible_subst_impl(map, filter, depth),
            Abs(a) => a.try_fallible_subst_impl(map, filter, depth),
            Var(v) => map(depth, v).map_err(Either::Left),
        }
    }
    fn lower_bound(&self) -> u64 {
        match self {
            Var(v) => *v,
            App(a) => a.lower_bound(),
            Abs(a) => a.lower_bound(),
        }
    }
    fn upper_bound(&self) -> u64 {
        match self {
            Var(v) => *v + 1,
            App(a) => a.upper_bound(),
            Abs(a) => a.upper_bound(),
        }
    }
    fn lower_bound_exact(&self) -> bool {
        false
    }
    fn upper_bound_exact(&self) -> bool {
        true
    }
}

impl<T> Substitute for App<T>
where
    T: Substitute<Term = T> + Hash + Clone,
    App<T>: Into<T>,
{
    /// The base term of this type
    type Term = T;
    /// The variable term of this type
    type Var = T::Var;
    /// The substitution error of this type
    type Error = T::Error;
    /// Attempt to recursively transform this term
    fn try_fallible_subst_impl<M, F, E>(
        &self,
        map: &mut M,
        filter: &mut F,
        depth: u64,
    ) -> Result<Option<Self::Term>, Either<E, T::Error>>
    where
        M: FnMut(u64, &T::Var) -> Result<Option<T::Term>, E>,
        F: FnMut(u64, &T::Term) -> Result<bool, E>,
    {
        let func = self.func.try_fallible_subst_impl(map, filter, depth)?;
        let arg = self.arg.try_fallible_subst_impl(map, filter, depth)?;
        if func.is_none() && arg.is_none() {
            return Ok(None);
        }
        let func: T = func.unwrap_or_else(|| self.func.clone());
        let arg: T = arg.unwrap_or_else(|| self.arg.clone());
        let result: App<T> = App::new(func, arg);
        Ok(Some(result.into()))
    }
    fn lower_bound(&self) -> u64 {
        self.lower_bound
    }
    fn upper_bound(&self) -> u64 {
        self.upper_bound
    }
}

impl Substitute for Abs {
    /// The base term of this type
    type Term = Term;
    /// The variable term of this type
    type Var = u64;
    /// The substitution error of this type
    type Error = Infallible;
    /// Attempt to recursively transform this term
    fn try_fallible_subst_impl<M, F, E>(
        &self,
        map: &mut M,
        filter: &mut F,
        depth: u64,
    ) -> Result<Option<Self::Term>, Either<E, Infallible>>
    where
        M: FnMut(u64, &u64) -> Result<Option<Term>, E>,
        F: FnMut(u64, &Term) -> Result<bool, E>,
    {
        let abs = self.abs.try_fallible_subst_impl(map, filter, depth + 1)?;
        if let Some(abs) = abs {
            let result = Abs::new(abs);
            Ok(Some(result.into()))
        } else {
            return Ok(None);
        }
    }
    fn lower_bound(&self) -> u64 {
        self.lower_bound
    }
    fn upper_bound(&self) -> u64 {
        self.upper_bound
    }
    fn lower_bound_exact(&self) -> bool {
        false
    }
    fn upper_bound_exact(&self) -> bool {
        true
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use parser::*;

    #[test]
    fn omega_reduces_to_itself() {
        for &mode in RMode::RMODES {
            if let Some(redex) = OMEGA.reduce_with(mode) {
                assert_eq!(*OMEGA, redex)
            }
        }
    }

    #[test]
    fn eta_reduction() {
        let pairs = [
            ("λ 1 0", Some("0")),
            ("λ 0", None),
            ("λ (λ 1) 0", None),
            ("λ (λ 0) 0", Some("λ 0")),
        ];
        for &(term, redex) in &pairs {
            let term = full_term(term);
            let redex = redex.map(full_term);
            assert_eq!(term.reduce_with(Eta), redex)
        }
    }

    #[test]
    fn beta_reduction() {
        let pairs = [
            ("(λ 0) 0", Some("0")),
            ("(λ 0) 1", Some("1")),
            ("(λ 1) 1", Some("0")),
            ("(λ 3) 1", Some("2")),
            ("(λ λ 1) 2 3", Some("(λ 3) 3")),
            ("(λ λ 0) (λ 5)", Some("λ 0")),
        ];
        for &(term_s, redex_s) in &pairs {
            let term = full_term(term_s);
            let redex = redex_s.map(full_term);
            assert_eq!(
                term.reduce_with(Beta),
                redex,
                "{} -𝛽> {}",
                term_s,
                redex_s.unwrap_or(term_s)
            )
        }
    }

    #[test]
    fn beta_normal_form() {
        let pairs = [
            ("(λ 0) 0", Some("0")),
            ("(λ 0) 1", Some("1")),
            ("(λ λ 1) 2 3", Some("2")),
            ("(λ λ 0) (λ 5)", Some("λ 0")),
        ];
        for &(term_s, redex_s) in &pairs {
            let term = full_term(term_s);
            let redex = redex_s.map(full_term);
            assert_eq!(
                term.normalize_with(Beta),
                redex,
                "{} -𝛽>> {}",
                term_s,
                redex_s.unwrap_or(term_s)
            )
        }
    }

    #[test]
    fn combinatory_algebra_subst() {
        let pairs = [
            ("0 1 1 2", Some("0")),
            ("0 2 3 4", Some("0 2 (1 2)")),
            ("0 4 3 2", Some("2 0 (1 0)")),
            ("1 2 3", Some("0")),
            ("1 3 2", Some("1")),
        ];
        for &(ski_s, redex_s) in &pairs {
            let ski_u = full_term(ski_s);
            let ski = ski_u
                .subst(
                    |depth, ix| {
                        if *ix < depth {
                            None
                        } else {
                            let ix = ix - depth;
                            match ix {
                                0 => Some(S.clone()),
                                1 => Some(K.clone()),
                                n => Some(Var(n - 2)),
                            }
                        }
                    },
                    |_, _| true,
                    0,
                )
                .unwrap_or(ski_u);
            let redex = redex_s.map(full_term);
            assert_eq!(ski.normalize_with(Beta), redex);
        }
    }
}
